package com.example.ps_proiect2.Model;

import com.google.gson.annotations.SerializedName;

import java.util.Observable;
import java.util.Observer;


public class Person implements Observer {
    @SerializedName("idperson")
    private Integer idperson;
    @SerializedName("username")
    private String username;
    @SerializedName("password")
    private String password;
    @SerializedName("role")
    private String role; //bucatar/user

    public Person() {
    }

    public Person(Integer idperson, String username, String password, String role) {
        this.idperson = idperson;
        this.username = username;
        this.password = password;
        this.role = role;
    }

    public Integer getIdperson() {
        return idperson;
    }

    public void setIdperson(Integer idperson) {
        this.idperson = idperson;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String name) {
        this.username = name;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "Person{" +
                "idperson=" + idperson +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", role='" + role + '\'' +
                '}';
    }

    @Override
    public void update(Observable o, Object arg) {
        System.out.println("Hei " + this.getIdperson() + " someone cooked " + ((Recipe) o).getName() + "<->" + arg);
    }
}