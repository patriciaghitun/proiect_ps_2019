package com.example.ps_proiect2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.example.ps_proiect2.Model.Recipe;
import com.example.ps_proiect2.Service.RecipeService;
import com.example.ps_proiect2.controller.RecipeController;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChefActivity extends AppCompatActivity {
    Button addRecipe;
    Button deleteRecipe;
    Button showRecipes;
    ListView recipeListView;

    RecipeController recipeController=new RecipeController();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chef_activity);

        recipeListView=findViewById(R.id.chef_recipes);

        addRecipe=findViewById(R.id.add_button);
        addRecipe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addNewRecipe();
            }
        });
        deleteRecipe=findViewById(R.id.delete_button);
        deleteRecipe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteExistentRecipe();
            }
        });
        showRecipes=findViewById(R.id.show_button);

        showRecipes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAllRecipes();
            }
        });
    }

    public void showAllRecipes(){
        recipeController.getAllRecipes(ChefActivity.this,recipeListView);
    }

    public void addNewRecipe(){
        Intent intent=new Intent(ChefActivity.this,AddDeleteActivity.class);
        startActivity(intent);
    }

    public void deleteExistentRecipe(){
        Intent intent=new Intent(ChefActivity.this,AddDeleteActivity.class);
        startActivity(intent);
    }
}
