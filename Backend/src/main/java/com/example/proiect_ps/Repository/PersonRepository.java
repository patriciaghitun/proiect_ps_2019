package com.example.proiect_ps.Repository;

import com.example.proiect_ps.Model.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface PersonRepository extends JpaRepository<Person,Integer> {

    @Query("SELECT p FROM Person p WHERE p.username=:givenUsername")
    Person getPersonByUsername(@Param("givenUsername") String givenUsername);

    @Query("SELECT p FROM Person p WHERE p.username=:givenUsername AND p.password=:givenPassword AND p.role=:givenRole")
    Person login(@Param("givenUsername") String givenUsername, @Param("givenPassword")String givenPassword, @Param("givenRole") String givenRole);

}
