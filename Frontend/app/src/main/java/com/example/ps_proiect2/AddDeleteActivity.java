package com.example.ps_proiect2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.ps_proiect2.Model.Recipe;
import com.example.ps_proiect2.controller.RecipeController;

public class AddDeleteActivity extends AppCompatActivity {
    Button addButton;
    Button deleteButton;

    RecipeController recipeController = new RecipeController();

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_delete_activity);

        addButton=findViewById(R.id.recipe_add_button);
        deleteButton=findViewById(R.id.recipe_delete_button);

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addButtonClicked();
            }
        });

        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deteleButtonClicked();
            }
        });
    }

    //merge blana
    public void addButtonClicked(){

        String recipeName = ((EditText)findViewById(R.id.recipe_add_name)).getText().toString().trim();
        String idChefString = ((EditText)findViewById(R.id.recipe_idchef)).getText().toString().trim();
        Integer idChefInteger = Integer.parseInt(idChefString);
        String recipeDescription = ((EditText)findViewById(R.id.recipe_description)).getText().toString().trim();
        Recipe recipe = new Recipe();
        recipe.setName(recipeName);
        recipe.setIdchef(idChefInteger);
        recipe.setDescription(recipeDescription);

        recipeController.addNewRecipe(AddDeleteActivity.this,recipe);
    }

    //nu merge dyeloc
    public void deteleButtonClicked(){
        String recipeId = ((EditText)findViewById(R.id.recipe_delete_id)).getText().toString().trim();
        Integer id=Integer.parseInt(recipeId);
        Toast.makeText(AddDeleteActivity.this,"Id-ul de sters este "+id,Toast.LENGTH_LONG).show();

        recipeController.deleteById(AddDeleteActivity.this,id);
    }
}
