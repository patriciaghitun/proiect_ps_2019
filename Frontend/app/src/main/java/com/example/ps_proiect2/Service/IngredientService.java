package com.example.ps_proiect2.Service;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class IngredientService {

    //private final static String API_URL = "http://192.168.0.101:8080/";
    //private final static String API_URL = "http://10.132.2.23:8080/";
    private final static String API_URL = "http://172.20.10.2:8080";

    private static IngredientApi ingredientApi;

    //get la instanta de retrofit
    public static IngredientApi getInstance() {
        if (ingredientApi == null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(API_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(provideOkHttp())
                    .build();

            ingredientApi = retrofit.create(IngredientApi.class);
        }
        return ingredientApi;
    }

    private static OkHttpClient provideOkHttp() {
        OkHttpClient.Builder httpBuilder = new OkHttpClient.Builder();
        httpBuilder.connectTimeout(30, TimeUnit.SECONDS);

        return httpBuilder.build();
    }
}
