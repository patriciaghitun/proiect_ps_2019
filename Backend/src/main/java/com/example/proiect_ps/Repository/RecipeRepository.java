package com.example.proiect_ps.Repository;

import com.example.proiect_ps.Model.Ingredient;
import com.example.proiect_ps.Model.Recipe;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;


public interface RecipeRepository extends JpaRepository<Recipe,Integer> {

    @Query("SELECT r FROM Recipe r WHERE r.idrecipe=:givenId")
    Recipe getRecipesByGivenId(@Param("givenId") Integer givenId);

    @Query("SELECT r.idchef FROM Recipe r WHERE r.idrecipe=:givenId")
    Integer findChefByRecipeId(@Param("givenId") Integer givenId);


    @Query("SELECT i FROM Recipe i WHERE i.idchef=:idChef")
    List<Recipe> getRecipesByChefId(@Param("idChef") Integer idChef);


    @Query("SELECT r FROM Recipe r WHERE r.name=:givenName")
    Recipe getRecipeByGivenName(@Param("givenName") String givenName);
}
