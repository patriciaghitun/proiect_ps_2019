package com.example.ps_proiect2.controller;


import android.content.Context;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.ps_proiect2.Model.Ingredient;
import com.example.ps_proiect2.Service.IngredientService;
import com.example.ps_proiect2.Service.RecipeService;
import com.example.ps_proiect2.UserActivity;

import java.util.List;

import okhttp3.Cookie;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class IngredientController {

    public void cookRecipe(final Context context, final Integer idRecipe) {
        final Call<Void> recipe = IngredientService.getInstance().cookRecipe(idRecipe);
        recipe.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Log.d("COOK", "on response");
                if(response.isSuccessful()){
                    Log.d("COOK", "successful");
                    Toast.makeText(context, "Congratulations! You just cooked recipe"+idRecipe, Toast.LENGTH_LONG).show();
                }
                else {
                    Log.d("COOK", "not sucessful");
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.d("COOK", "on failure");
            }
        });
    }
}
