package com.example.proiect_ps.Repository;

import com.example.proiect_ps.Model.Chef;
import com.example.proiect_ps.Model.Ingredient;
import com.example.proiect_ps.Model.Person;
import com.example.proiect_ps.Model.Recipe;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

public interface IngredientRepository extends JpaRepository<Ingredient,Integer> {

    @Query("SELECT i FROM Ingredient i WHERE i.name=:givenName")
    Ingredient getIngredientByName(@Param("givenName") String givenName);

    //get the ingredients of a recipe
    @Query("SELECT i.idingredient FROM Ingredient i WHERE i.idrecipe=:givenId")
    List<Integer> getIngredientsByRecipe(@Param("givenId") Integer givenId);

    @Query("SELECT i FROM Ingredient i WHERE i.idrecipe=:givenId")
    List<Ingredient> getIngredientsObjectsByRecipe(@Param("givenId") Integer givenId);

    //find recipe by id
    @Query("SELECT r FROM Recipe r WHERE r.idrecipe=:givenId")
    Recipe getRecipe(@Param("givenId") Integer givenId);

    //find chef by id
    @Query("SELECT p FROM Person p WHERE p.idperson=:givenId")
    Person getChefById(@Param("givenId") Integer givenId);//dau chef-ul doar pt ca eu voi da mereu un id de chef

    @Query("SELECT i.idrecipe FROM Ingredient i WHERE i.idingredient=:givenId")
    Integer getRecipeIdFromIngredientId(@Param("givenId") Integer givenId);


}
