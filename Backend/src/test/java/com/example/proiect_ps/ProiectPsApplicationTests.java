package com.example.proiect_ps;

import com.example.proiect_ps.Model.PersonFactory;
import com.example.proiect_ps.Model.PersonType;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProiectPsApplicationTests {

	@Mock
	PersonFactory personFactory ;
	PersonType type1;
	PersonType type2;

	@Before
	public void init() {
		personFactory= new PersonFactory();
	}

	@Test
	public void testUser(){

		//verificare pentru USER
		type1 = personFactory.createPerson("user");
		System.out.println("print : "+type1.getClass().getSimpleName());
		assertEquals("User",type1.getClass().getSimpleName());
	}

	@Test
	public void testChef(){

		//verificare pentru CHEF
		type2 = personFactory.createPerson("chef");
		System.out.println("print : "+type2.getClass().getSimpleName());
		assertEquals("Chef",type2.getClass().getSimpleName());
	}

}

