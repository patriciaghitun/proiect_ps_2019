package com.example.proiect_ps.Controller;

import com.example.proiect_ps.Model.Ingredient;
import com.example.proiect_ps.Model.Person;
import com.example.proiect_ps.Model.Recipe;
import com.example.proiect_ps.Repository.IngredientRepository;
import com.example.proiect_ps.Repository.RecipeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.List;

@RestController
@RequestMapping(path="/ingredient")

public class IngredientController {
    @Autowired
    private IngredientRepository ingredientRepository;
    private RecipeRepository recipeRepository;

    /**
     * Find a specific ingredinet by an id (string) in the body of the request
     * @param idIngredient
     * @return A string response in order to know if the specified ingredient is present or not in the database.
     */
    @PostMapping(path="/findById/{idIngredient}")
    public String findIngredientById(@PathVariable Integer idIngredient){
        if(ingredientRepository.findById(idIngredient).isPresent())
        {
            return "Ingredient found";
        }else return "Ingredient not found";
    }

    /**
     * Finds all the ingredients present in the database
     * @return List of Iterable Ingredient objects that are present in the database at the time.
     */
    @GetMapping(path="/all")
    public @ResponseBody Iterable<Ingredient> getAllIngredients() {
        return ingredientRepository.findAll();
    }

    /**
     * Get the ingredients of a recipe by giving its id.
     * @param idRecipe
     * @return String - list of string ingredient ids
     */
    @GetMapping(path="/getIngredientsByIdRecipe/{idRecipe}")
    public StringBuilder getIngredientsByIdRecipe(@PathVariable Integer idRecipe){
        List<Integer> ingredientsList = ingredientRepository.getIngredientsByRecipe(idRecipe);
        StringBuilder stringBuilder = new StringBuilder("");
        for(Integer i : ingredientsList)
            stringBuilder.append(i);
        return stringBuilder;
    }


    @GetMapping(path="/getIngredientsObjectsByIdRecipe/{idRecipe}")
    public List<Ingredient> getIngredientsObjectsByIdRecipe(@PathVariable Integer idRecipe){
        List<Ingredient> ingredientsList = ingredientRepository.getIngredientsObjectsByRecipe(idRecipe);
        return ingredientsList;
    }

    @GetMapping(path = "/getIngredientByName/{givenName}")
    public Ingredient getIngredientByName(@PathVariable String givenName){
        Ingredient ingredient = ingredientRepository.getIngredientByName(givenName);
        return ingredient;
    }

    //Pentru observer

    /**
     * This method is used mostly for the implementation of the Observer Design Pattern
     * Using this, we "cook" a recipe with a given Id. First, we get the ingredients of that specific recipe and then the chef that
     * created that recipe. In the end, we notify the chef that someone cooked his recipe .
     * @param idRecipe
     */
    @PutMapping(path="/cookRecipe/{idRecipe}")
    public void cookRecipe(@PathVariable Integer idRecipe){

        //efecitv se face o reteta si atunci voi notifica persoanele care au facut reteta aceasta
        List<Ingredient> ingredients = getIngredientsObjectsByIdRecipe(idRecipe);
        System.out.println("HEI");

        Recipe recipe=ingredientRepository.getRecipe(idRecipe);
        System.out.println("RETETA : " + recipe.getName()+ " cu CHEF : " +recipe.getIdchef());
        Person chef = ingredientRepository.getChefById(recipe.getIdchef());
        System.out.println("CHEF : "+chef.getUsername());
        chef.update(recipe,"someone implemented your recipe mr - "+chef.getIdperson());
    }

    @GetMapping(path = "/getRecipeIdFromIngredientId/{givenId}")
    public Integer getRecipeIdFromIngredientId(@PathVariable Integer givenId){
        return ingredientRepository.getRecipeIdFromIngredientId(givenId);
    }

    /**
     * Deletes an ingredient by its Id(string in the RequestBody)
     * @param idIngredient
     * @return A response for the user to know if the ingredient was deleted or it didn't exist in the first place in our database.
     */
    @DeleteMapping(path="/delById")
    public String deleteIngredientById(@RequestBody String idIngredient){
        int id=Integer.parseInt(idIngredient);
        if(findIngredientById(id).equals("Ingredient found"))
        {
            ingredientRepository.deleteById(id);
            return "Ingredient deleted";
        }else return "Ingredient not found in the database";
    }

    @DeleteMapping(path="/deleteById/{idingredient}")
    public String deleteById(@PathVariable Integer idingredient){
        if(findIngredientById(idingredient).equals("Ingredient found"))
        {
            ingredientRepository.deleteById(idingredient);
            return "Ingredient deleted";
        }else return "Ingredient not found in the database";
    }

}