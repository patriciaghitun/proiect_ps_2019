package com.example.ps_proiect2.Service;


import com.example.ps_proiect2.Model.Ingredient;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface IngredientApi {
    @GET("/ingredient/all")// exact de unde vreau eu sa iau
    Call<List<Ingredient>> getIngredients();

    @GET("/ingredient/getIngredientByName/{givenName}")
    Call<Ingredient> getIngredientByName(@Path("givenName") String givenName);

    @GET("/ingredient/getRecipeIdFromIngredientId/{givenId}")
    Call<Integer> getRecipeIdFromIngredientId(@Path("givenId") Integer givenId);

    @PUT("/ingredient/cookRecipe/{idRecipe}")
    Call<Void> cookRecipe(@Path("idRecipe") Integer idRecipe);
}
