package com.example.ps_proiect2;

import com.example.ps_proiect2.Model.Person;

class User extends Person {
    public User(){
        super.setRole("user");
    }
    public User(Integer idperson, String username,String pass){
        super(idperson,username,pass,"user");
    }
}
