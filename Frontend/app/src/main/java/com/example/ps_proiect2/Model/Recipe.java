package com.example.ps_proiect2.Model;

import com.google.gson.annotations.SerializedName;

import java.util.Observable;

public class Recipe extends Observable {

    @SerializedName("idrecipe")
    private Integer idRecipe;
    @SerializedName("name")
    private String name;
    @SerializedName("idchef")
    private int idchef;
    @SerializedName("description")
    private String description;


    public Recipe(){}

    public Recipe(Integer idrecipe, String name, String description, int idchef) {
        this.idRecipe = idrecipe;
        this.name = name;
        this.description = description;
        this.idchef = idchef;
    }

    public Integer getIdrecipe() {
        return idRecipe;
    }

    public void setIdrecipe(Integer idrecipe) {
        this.idRecipe = idrecipe;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getIdchef() {
        return idchef;
    }

    public void setIdchef(int idchef) {
        this.idchef = idchef;
    }
}
