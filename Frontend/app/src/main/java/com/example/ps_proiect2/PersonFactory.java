package com.example.ps_proiect2;

import com.example.ps_proiect2.Model.Person;

class PersonFactory {
    public static Person createPerson(String type){
        if(type.equals("user")){
            return new User();
        }else if(type.equals("chef")){
            return new Chef();
        }else{
            return null;
        }
    }
}
