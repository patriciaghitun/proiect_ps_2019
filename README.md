DOCUMENTATIE

## 1. Descriere proiect

Aplicatia mea Prin intermediul unei interfete utilizator prietenoasa si usor de utilizat, o persoana isi poate crea un cont de tip USER sau CHEF. 
Daca persoana este USER atunci ea foloseste aplicatia cu scopul ei principal de a introduce un ingredient dorit si a primi ca rezultat afisat 
o reteta ce contine acest ingredient. 
Daca persoana doreste sa isi faca un cont de tip CHEF atunci are posibilitatea de a adauga retete proprii, de a sterge unele retete si 
de asemenea de a vizualiza toate retetele disponibile.
In momentul in care un user doreste sa "gateasca" o reteta acesta trebuie sa introduca date despre reteta respectiva si prin apasarea unui simplu buton,
se va apela o functie pentru aceasta optiune.
---

## 2. Proiectare

Proiectare baza de date Am folosit baze de date pentru stocarea datelor despre o persoane, ingrediente si retete. 
Acestea din urma au devenit astfel, tabele in baza de date. Relatiile dintre tabele au fost de "one to many", 
deoarece am dezvoltat aplicatia pe urmatorul principiu : -> O persoana introduce un ingredient - trebuie sa i se afiseze o reteta 
ce contine acel ingredient -> Un ingredient apartine unei singure retete , iar o reteta poate avea mai multe ingrediente. 
Aceasta relatie am implementat-o folosind un idreteta in tabela de ingredient pentru a retine reteta din care face parte.

---

## 3. Implementare

Am folosit o arhitectura bazate pe pachete pentru implementarea aplicatiei, precum : Model, Controller, Repository. 
Pachetul Model contine clasele corespunzatoare tabelelor din baza de date : Person si Ingredient. 
Pachetul Controler contine o serie de metode implementate pentru a comunica cu baza de date : endpoint-urile. 
Pachetul Repository contine doua interfete PersonRepository si IngredientRepository care extind fiecare clasa CrudRepository pt comunicarea cu baza de date.

Am folosit o aplicatie de tip server pentru a crea serviciile pentru comunicarea cu baza de date pentru partea de back end si 
una de tip client pentru partea de front end. Clientul a fost implementat in Android Studio folosind aplicatia pentru mobil. 
Serviciile suplimentare dar necesare comunicarii cu baza de date pentru anumite operatii au fost implementate cu ajutorul query-urilor sql. 
De exemplu, pentru a gasi o reteta pe baza id-ului ingredientului care face parte din ea, am implementat un query prin care sa imi returneze 
idrecipe cand eu transmit ca parametru id ingredient.

Conexiunea la baza de date a fost facuta cu ajutorul SpringBoot si testata cu programul Postamn. Tot cu ajutorul acestui program au fost testate initial
serviciile de care va dispune aplicatia server.

## 4. Design Patterns

Ca si design pattern-uri implementate, am folosit Factory Design Pattern si Observer Design Pattern.
Am folosit Factory Design Pattern pentru crearea unui nou user sau a unui chef in functie de ce rol doreste (un string dat de catre user in aplicatia client) sa indeplineasca persoana in momentul 
in care se inregistreaza pentru prima data. 
De asemenea, am folosit Observer Design Pattern pentru a notifica un chef in momentul in care un user doreste sa gateasca reteta creata si postata de acesta.

---
