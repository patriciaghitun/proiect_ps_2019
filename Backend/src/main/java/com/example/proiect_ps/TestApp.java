package com.example.proiect_ps;
import com.example.proiect_ps.Model.PersonFactory;
import com.example.proiect_ps.Model.PersonType;

public class TestApp {
    public static void main(String[] args) {
        //test la factory
        PersonType newPerson = PersonFactory.createPerson(PersonType.USER);
        System.out.println(newPerson.getClass().getSimpleName());
    }
}
