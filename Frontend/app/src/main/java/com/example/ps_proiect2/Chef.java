package com.example.ps_proiect2;

import com.example.ps_proiect2.Model.Person;

class Chef extends Person {
    private int nrRecipes;

    public Chef(){
        super.setRole("chef");
    }

    public Chef(Integer idperson, String username,String pass, int nrRecipes) {
        super(idperson,username,pass,"chef");
        this.nrRecipes = nrRecipes;
    }
}
