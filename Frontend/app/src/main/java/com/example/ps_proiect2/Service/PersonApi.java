package com.example.ps_proiect2.Service;

import com.example.ps_proiect2.Model.Person;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface PersonApi {

    @GET("/person/all")
    Call<List<Person>> getPersons();

    @GET("/person/getPersonByUsername/{username}")
    Call<Person> getPersonByUsername(@Path("username") String username);

    @GET("person/login/{username}/{password}/{role}")
    Call<Person> login(@Path("username") String username, @Path("password") String password, @Path("role") String role);

    @Headers("Accept: application/json")
    @POST("/person/add")
    Call<Person> addNewPerson(@Body Person person);
}
